<?php
/**
 * Created by PhpStorm.
 * User: xilos
 * Date: 14.06.2019
 * Time: 20:45
 */

namespace App\Order;

use App\Admin\Frontend\Importer\OrderItemToSave;
use App\Currency\ICurrencyService;

interface IOrderService
{
    /**
     * Считает итоговую сумму заказа.
     *
     * @param OrderItemToSave[] $itemsToSave
     * @param int $currencyId
     * @return float
     */
    public function countTotalByItemsToSave($itemsToSave, $currencyId);
}