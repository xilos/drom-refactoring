<?php
/**
 * Created by PhpStorm.
 * User: xilos
 * Date: 14.06.2019
 * Time: 20:45
 */

namespace App\Order;

use App\Admin\Frontend\Importer\OrderItemToSave;
use App\Currency\ICurrencyService;

/**
 * Сервис управления заказами
 */
class OrderService implements IOrderService
{
    /**
     * @var ICurrencyService
     */
    private $currencyService;

    /**
     * @param ICurrencyService $currencyService
     */
    public function __construct(
      ICurrencyService $currencyService
    ) {
      $this->currencyService = $currencyService;
    }

    /**
     * Считает итоговую сумму заказа.
     *
     * @inheritdoc
     */
    public function countTotalByItemsToSave($itemsToSave, $currencyId)
    {
      if (empty($itemsToSave)) {
        return 0.0;
      }

      $total = 0;
      foreach ($itemsToSave as $itemToSave) {
        if ($itemToSave->isCanceled()) {
          continue;
        }

        $total += $itemToSave->getPrice() * $itemToSave->getQuantity();
      }

      return $this->currencyService->roundCurrency($total, $currencyId);
    }
}